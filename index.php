<?php
require_once ('animal.php') ;
require_once ('frog.php') ;
require_once ('ape.php') ;

$sheep = new Animal ("shaun") ;
echo "Name :" . $sheep->name . "<br>";
echo "Legs :" . $sheep->legs. "<br>" ; 
echo "Cold_Blooded :" .  $sheep->cold_blooded. "<br>"; 

echo "<br> " ;
$kodok= new frog ("buduk") ;
echo "Name :" . $kodok->name . "<br>";
echo "Legs : " . $kodok->legs. "<br>" ; 
echo "Cold_Blooded :".  $kodok->cold_blooded. "<br>";
echo "Jump : " . $kodok->jump(). "<br>" ;

echo "<br>" ;

$sunggokong= new ape ("kera sakti") ;
echo "Name :". $sunggokong->name . "<br>" ;
echo "Legs: " . $sunggokong->legs . "<br>" ;
echo "Cold_Blooded : ". $sunggokong->cold_blooded . "<br>" ;
echo "Yell : ". $sunggokong->yell() ;

?>
